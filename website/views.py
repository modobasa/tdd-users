from flask import Blueprint, render_template, request, flash, redirect, url_for
from flask_login import login_required, current_user
from .models import User, UserRepository, EnableSupplier
from . import db

views = Blueprint("views", __name__)

@views.route("/")
@views.route("/home")
@login_required
def home():    
    return render_template("home.html", user=current_user)


@views.route("/users")
@login_required
def users():
    # users = User.query.all()
    users = UserRepository.get_all()
    return render_template("users.html", user=current_user, users=users)


@views.route("/delete-user/<id>", methods=['GET', 'POST'])
@login_required
def delete_user(id, action=None):
    delete_user = UserRepository()
    user = delete_user.get(id)
    if not user:
        
        flash("User does not exist!", category='error')
        
    else:        
        if request.method == "POST":                
            db.session.delete(user)
            db.session.commit()
            flash('User deleted.', category='success')
            return redirect(url_for('views.users'))
        return render_template("delete_user.html", user=current_user, users=user, id=id)

    return redirect(url_for('views.users'))


@views.route("/edit-user/<id>", methods=['GET', 'POST'])
@login_required
def edit_user(id):
    user = UserRepository()
    edit_user = user.get(id)        
    user_details = edit_user.user_variables()        
    if request.method == 'POST': 
        updated_values_dict = request.form.to_dict()
        checkbox = request.form.get("update_is_active")
        for k, v in updated_values_dict.items():                        
            if k == 'update_id':
                edit_user.id = v.strip()
            if k == 'update_username':
                edit_user.username = v.strip()
            if k == 'update_name':
                edit_user.name = v.strip()
            if k == 'update_email':
                edit_user.email = v.strip()                                 
            if checkbox is None:                
                edit_user.disable()                
            else:
                # edit_user.enable()
                EnableSupplier.is_active = True

        db.session.commit()        
        return redirect(url_for('views.users'))
    return render_template('edit_user.html', user=current_user, user_details=user_details)


