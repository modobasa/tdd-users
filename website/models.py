import abc
from . import db
from flask_login import UserMixin
from datetime import datetime
from sqlalchemy.sql import func
from dataclasses import dataclass



class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(150), unique=True)
    username = db.Column(db.String(150), unique=True)
    name = db.Column(db.String(150))
    password = db.Column(db.String(150))    
    is_active = db.Column(db.Boolean, nullable=False, default=False)
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    date_updated = db.Column(db.DateTime, default=db.func.current_timestamp(), onupdate=db.func.current_timestamp())

    def __str__(self) -> str:
        return self.username

    def __repr__(self):
       return f"<id={self.id}, username={self.username}>"
    
    def user_variables(self):

        user_details = {
            'Id:': ['id', self.id],
            'Username:': ['username', self.username],
            'Name:': ['name', self.name],
            'Email:': ['email', self.email],
            'Account Status': ['is_active', self.is_active],
             }
        return user_details
   
    def disable(self):
        self.is_active = False       
    
    def enable(self):
        self.is_active = True


class UserRepository(User):
    def init(self, session):        
        self._session = session

    def get(self, id):
        # user = User.query.filter_by(id=id).first()
        user = User.query.get_or_404(id)
        return user

    def get_all():
        return User.query.all()
    
    def get_by_name(self, name):
        return User.query.filter_by(name=name)

"""
Cum ar arata flow-ul de a face enable la un user de exemplu:
a. utilizatorul apasa butonul in UI si se face call la
   server (again, JS sau refresh - don't care)

b. Flask vede request-ul si creaza o comanda EnableSupplier. O comanda nu este decat un dataclass,
   ex: class EnableSupplier: email: str

c. Aceasta comanda este transmisa catre un MessageBus
   care face legatura cu un handler d. Se apeleaza handler-ul (EnableSupplierHandler - care este de fapt o functie) care,
   folosind repo-ul, gaseste user-ul e. Odata gasit user-ul se apeleaza metoda enable

"""
@dataclass
class EnableSupplier():
    id: int
    email: str
    is_active: bool    

