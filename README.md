Marius App:

Link catre carte: https://www.cosmicpython.com/book/preface.html

O aplicatie simpla care sa afiseze o lista de utilizatori.
Cerinte:
* o pagina care sa listeze utilizatorii
* o pagina de adaugare a unui nou utilizator
* posibilitatea de a edita un utilizator existent
* optiunea de a face enable/disable din lista de utilizatori

Notes:
#. Cum arata UI-ul nu conteaza. Se pot face request-uri JS sau refresh la pagina. Does not matter!
#. Sa se foloseasca un ORM (SQLAlchemy)
#. Sa fie folosit CQRS (Command Query Responsibility Segretation) principle
#. Am stabilit ca folosim Flask ca framework
#. Sa folosim Pytest pentru teste

Guidelines
1. Modelele sunt clase care mapeaza un rand in baza de date. 
De exemplu, pentru un row din tabela users ce are coloanele `id,first_name,last_name,email,is_active`
clasa ar arata ceva de genu':
```
class User:
    def __init__(self, first_name, last_name, email, enabled):
        self.first_name = first_name
        self.last_name = first_name
        self.email = email
        self.is_enabled = enabled
```
2. Modele ar trebui sa contina logica de business ce tine de ele. De exemplu, actiunea de a face enable/disable la un user ar trebui sa fie pe modelul respectiv.
```
class User:
    ...

    def disable(self):
        self.is_enabled = False

    def enable(self):
        self.is_enabled = True
```
3. Managementul modelelor sa fie facut de un repository. Exemplu:
class UserRepository:
    def __init__(self, session):
        self._session = session  # conexiunea la baza de date
    def get(self, email):
        # query care sa caute un user dupa email

Cu alte cuvinte: sa fie o separare intre domain (clasa User) si persistence layer (Repository).

4. Cum ar arata flow-ul de a face enable la un user de exemplu:
    a. utilizatorul apasa butonul in UI si se face call la server (again, JS sau refresh - don't care)
    b. Flask vede request-ul si creaza o comand EnableSupplier. O comanda nu este decat un dataclass, ex:
        class EnableSupplier:
            email: str
    c. Aceasta comanda este transmisa catre un MessageBus care face legatura cu un handler
    d. Se apeleaza handler-ul (EnableSupplierHandler - care este de fapt o functie) care, folosind repo-ul, gaseste user-ul
    e. Odata gasit user-ul se apeleaza metoda `enable`

5. Testarea unitara este suficienta pentru moment. Ce ar trebui sa testam insa? Flow-urile. Nu testam modele. Exemplu:
```
class User:
    ...
    def enable(self):
        self.is_enabled = True
    ...
```
Nu facem un test care sa creeze un User, sa apeleze user.enable() si apoi sa verifice ca `is_enabled` este `True`. In schimb, exista flow-ul de a face enable la user si acesta este cel pe care trebuie sa-l testam.
So, testul nostru (again unit test) ar face urmatorii pasi:
    a. adauga user in baza de date cu `is_enabled = False`
    b. creaza o comanda `EnableSupplier` cu email-ul user-ului de la pct a.
    c. apeleaza handler-ul (functia) `EnableSupplierHandler` cu aceasta comanda
    c. se cauta din nou userul in baza de date si se verifica ca is_enabled este acum True
    * fiind un unit test, baza de date este fake si nu interactionam cu o baza de date reala. Exemple si instructiuni sunt in carte.

De fapt, tot ce este mentionat aici este in carte. Acesta este doar un overview.
