import unittest
import sys

try:
    sys.path.insert(0, 'C:\\Users\\modobasa\\Documents\\Python\\TDD-Users\\src\\')       
    from website.models import User    
except:
    print("Error loading module!")


class TestUsers(unittest.TestCase):   
    
    def test_if_is_any_instance(self):     
        result = User.disable(self)
        self.assertIsInstance(result,User)
    
    def test_if_is_any_user_defined_as_admin(self):               
        result = User.query.filter_by(id=id)
        self.assertEqual(result.username, "admin")

    def test_if_user_is_active(self):
        result = User.disable(self)
        result.is_active = False
        self.assertEqual(result.is_active, False)        

    def test_number_of_instances(self):
        result = User.load(self)               
        self.assertEqual(result.id, 1)

if __name__ == '__main__':
    unittest.main()
